﻿namespace Paises
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using Modelos;
    using Servicos;
    using System.Threading.Tasks;
    using System.Net.Http;
    using Newtonsoft.Json;
    using System.Linq;

    public partial class Form1 : Form
    {
        #region atributos

        public List<Pais> paises = new List<Pais>();
        
        private NetworkService networkservice;

        private ApiService apiService;

        private DialogService dialogService;

        private DataService dataService;

        #endregion

        public Form1()
        {
            InitializeComponent();
          
            networkservice = new NetworkService();
            apiService = new ApiService();
            dialogService = new DialogService();
            dataService = new DataService();
           
            LoadPaises();
                     
        }

        private async void LoadPaises()
        {
            bool load;

            //ProgressBar1.Value = 0;       nao e necessario pq por defeito vem nas properties 

            LabelResultado.Text = "A atualizar paises...";

            var connection = networkservice.CheckConnection();

            if (!connection.IsSuccess)
            {
                LoadLocalPaises();
                load = false;
            }
            else
            {
                LoadApiPaises();
               
                load = true;

            }
            try
            {
                
                
                if (paises.Capacity != 0)
                {
                    LabelResultado.Text = "Não há ligação á internet e não foram préviamente carregados" + Environment.NewLine + "Tente mais tarde!";


                    return;
                }
               
            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }  

            LabelResultado.Text = "Paises atualizados";

            if (load)
            {
                LabelResultado.Text = string.Format("Paises carregados da internet em {0:F}", DateTime.Now);
            }
            else
            {
                LabelResultado.Text = string.Format("Paises carregados da base de dados.");
            }

            ProgressBar1.Value = 100;
            
        }

        private void LoadLocalPaises()
        {
            paises = dataService.GetData();
        }

        /*  private static async Task LoadApiPaises()
          {
           

                 // var response = await apiService.GetPaises("https://restcountries.eu", "/rest/v2/all");
                 // response.Result.ToString();
                  var cliente = new HttpClient();
                  cliente.BaseAddress = new Uri("https://restcountries.eu");
                  HttpResponseMessage response = await cliente.GetAsync("rest/v2/all");
                  string result = await response.Content.ReadAsStringAsync();
                  MessageBox.Show( JsonConvert.DeserializeObject(result));
                  if (!response.IsSuccessStatusCode)
                  {
                      return;
                  }

                        List<Pais> L = JsonConvert.DeserializeObject<List<Pais>>(result);
              foreach (var item in L)
                  {

                      if (item != null)
                      {

                      }
                  }
             
          }*/


        
        public async Task LoadApiPaises()
        {
            

            ProgressBar1.Value = 0;
            var cliente = new HttpClient();
            cliente.BaseAddress = new Uri("https://restcountries.eu");
            HttpResponseMessage response = await cliente.GetAsync("rest/v2/all");
            string result = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {
                return;
            }

            List<Pais> L = JsonConvert.DeserializeObject<List<Pais>>(result); //converter o que recebe para a lista
            

            foreach (var pais in L)
                {
                    if (pais != null)
                    {
                       
                         paises.Add(pais);
                    ComboBoxPaises.Items.Add(pais.name);

                }
                }
            foreach (var item in paises)
            {
              
            }

            dataService.DeleteData();
            dataService.SaveData(paises);
        }

        private void ComboBoxPaises_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListViewDominio.Clear();
            lblMoeda.Text = String.Empty;
            IEnumerable<Pais> Qimg;
            try
            {
                Qimg = from paises in paises
                       where paises.name == ComboBoxPaises.SelectedItem.ToString()
                       select paises;
            }
            catch (Exception)
            {

                throw;
            }
            foreach (var item in Qimg)
            {

                LabelCapital.Text = item.capital;
                LabelRegiao.Text = item.region;
                LabelSubRegiao.Text = item.subregion;
                LabelPopulacao.Text = item.population.ToString();
                LabelArea.Text = item.area.ToString();
                foreach (var a in item.topLevelDomain)
                {
                    // ListViewDominio.Items.Add(a);
                    //  ListViewDominio.
                    //  var listViewItem = new ListViewItem();
                    // ListViewDominio.Items.Add(listViewItem);
                    ListViewItem item1 = new ListViewItem(a);
                    ListViewDominio.Items.AddRange(new ListViewItem[] { item1 });
                }
                foreach (Currency b in item.currencies)
                {
                    lblMoeda.Text += b.name;


                }
            }



           
        }

      
    }
}
