﻿namespace Paises
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ComboBoxPaises = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LabelRegiao = new System.Windows.Forms.Label();
            this.LabelCapital = new System.Windows.Forms.Label();
            this.ProgressBar1 = new System.Windows.Forms.ProgressBar();
            this.LabelResultado = new System.Windows.Forms.Label();
            this.ListViewDominio = new System.Windows.Forms.ListView();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.LabelSubRegiao = new System.Windows.Forms.Label();
            this.a = new System.Windows.Forms.Label();
            this.b = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.LabelPopulacao = new System.Windows.Forms.Label();
            this.LabelArea = new System.Windows.Forms.Label();
            this.lblMoeda = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ComboBoxPaises
            // 
            this.ComboBoxPaises.FormattingEnabled = true;
            this.ComboBoxPaises.Location = new System.Drawing.Point(81, 31);
            this.ComboBoxPaises.Name = "ComboBoxPaises";
            this.ComboBoxPaises.Size = new System.Drawing.Size(189, 21);
            this.ComboBoxPaises.TabIndex = 0;
            this.ComboBoxPaises.SelectedIndexChanged += new System.EventHandler(this.ComboBoxPaises_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 238);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Capital:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 204);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Região:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Paises";
            // 
            // LabelRegiao
            // 
            this.LabelRegiao.AutoSize = true;
            this.LabelRegiao.Location = new System.Drawing.Point(78, 204);
            this.LabelRegiao.Name = "LabelRegiao";
            this.LabelRegiao.Size = new System.Drawing.Size(10, 13);
            this.LabelRegiao.TabIndex = 6;
            this.LabelRegiao.Text = "-";
            // 
            // LabelCapital
            // 
            this.LabelCapital.AutoSize = true;
            this.LabelCapital.Location = new System.Drawing.Point(78, 238);
            this.LabelCapital.Name = "LabelCapital";
            this.LabelCapital.Size = new System.Drawing.Size(10, 13);
            this.LabelCapital.TabIndex = 7;
            this.LabelCapital.Text = "-";
            // 
            // ProgressBar1
            // 
            this.ProgressBar1.Location = new System.Drawing.Point(12, 335);
            this.ProgressBar1.Name = "ProgressBar1";
            this.ProgressBar1.Size = new System.Drawing.Size(573, 23);
            this.ProgressBar1.TabIndex = 9;
            // 
            // LabelResultado
            // 
            this.LabelResultado.AutoSize = true;
            this.LabelResultado.Location = new System.Drawing.Point(27, 295);
            this.LabelResultado.Name = "LabelResultado";
            this.LabelResultado.Size = new System.Drawing.Size(60, 13);
            this.LabelResultado.TabIndex = 10;
            this.LabelResultado.Text = "Resultados";
            // 
            // ListViewDominio
            // 
            this.ListViewDominio.Location = new System.Drawing.Point(30, 103);
            this.ListViewDominio.Name = "ListViewDominio";
            this.ListViewDominio.Size = new System.Drawing.Size(162, 45);
            this.ListViewDominio.TabIndex = 11;
            this.ListViewDominio.UseCompatibleStateImageBehavior = false;
            this.ListViewDominio.View = System.Windows.Forms.View.List;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Dominio";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(230, 77);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Moeda";
            // 
            // LabelSubRegiao
            // 
            this.LabelSubRegiao.AutoSize = true;
            this.LabelSubRegiao.Location = new System.Drawing.Point(361, 204);
            this.LabelSubRegiao.Name = "LabelSubRegiao";
            this.LabelSubRegiao.Size = new System.Drawing.Size(0, 13);
            this.LabelSubRegiao.TabIndex = 15;
            // 
            // a
            // 
            this.a.AutoSize = true;
            this.a.Location = new System.Drawing.Point(303, 238);
            this.a.Name = "a";
            this.a.Size = new System.Drawing.Size(58, 13);
            this.a.TabIndex = 16;
            this.a.Text = "População";
            // 
            // b
            // 
            this.b.AutoSize = true;
            this.b.Location = new System.Drawing.Point(332, 268);
            this.b.Name = "b";
            this.b.Size = new System.Drawing.Size(29, 13);
            this.b.TabIndex = 17;
            this.b.Text = "Area";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(301, 204);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "SubRegiao";
            // 
            // LabelPopulacao
            // 
            this.LabelPopulacao.AutoSize = true;
            this.LabelPopulacao.Location = new System.Drawing.Point(363, 238);
            this.LabelPopulacao.Name = "LabelPopulacao";
            this.LabelPopulacao.Size = new System.Drawing.Size(10, 13);
            this.LabelPopulacao.TabIndex = 19;
            this.LabelPopulacao.Text = "-";
            // 
            // LabelArea
            // 
            this.LabelArea.AutoSize = true;
            this.LabelArea.Location = new System.Drawing.Point(367, 268);
            this.LabelArea.Name = "LabelArea";
            this.LabelArea.Size = new System.Drawing.Size(10, 13);
            this.LabelArea.TabIndex = 20;
            this.LabelArea.Text = "-";
            // 
            // lblMoeda
            // 
            this.lblMoeda.AutoSize = true;
            this.lblMoeda.Location = new System.Drawing.Point(230, 103);
            this.lblMoeda.Name = "lblMoeda";
            this.lblMoeda.Size = new System.Drawing.Size(10, 13);
            this.lblMoeda.TabIndex = 21;
            this.lblMoeda.Text = "-";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(597, 390);
            this.Controls.Add(this.lblMoeda);
            this.Controls.Add(this.LabelArea);
            this.Controls.Add(this.LabelPopulacao);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.b);
            this.Controls.Add(this.a);
            this.Controls.Add(this.LabelSubRegiao);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ListViewDominio);
            this.Controls.Add(this.LabelResultado);
            this.Controls.Add(this.ProgressBar1);
            this.Controls.Add(this.LabelCapital);
            this.Controls.Add(this.LabelRegiao);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ComboBoxPaises);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ComboBoxPaises;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label LabelRegiao;
        private System.Windows.Forms.Label LabelCapital;
        private System.Windows.Forms.ProgressBar ProgressBar1;
        private System.Windows.Forms.Label LabelResultado;
        private System.Windows.Forms.ListView ListViewDominio;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label LabelSubRegiao;
        private System.Windows.Forms.Label a;
        private System.Windows.Forms.Label b;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label LabelPopulacao;
        private System.Windows.Forms.Label LabelArea;
        private System.Windows.Forms.Label lblMoeda;
    }
}

