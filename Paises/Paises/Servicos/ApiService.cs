﻿using Newtonsoft.Json;
using Paises.Modelos;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Paises.Servicos
{
    using Newtonsoft.Json;
    using Modelos;
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;

    public class ApiService
    {
        public async Task<Response> GetPaises(string urlBase,string controller)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);

                var response = await client.GetAsync(controller);

                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }

                var paises = JsonConvert.DeserializeObject<List<Pais>>(result);


                //Processo para substituir os null por N/A
                foreach (var pais in paises)
                {
                    if (pais.capital == null)
                        pais.capital = "N/A";

                    if (pais.flag == null)
                        pais.flag = "N/A";

                    if (pais.name == null)
                        pais.name = "N/A";

                    if (pais.region == null)
                        pais.region = "N/A";

                    foreach (var moedas in pais.currencies)
                    {
                        if (moedas.name == null)
                            moedas.name = "N/A";
                        if (moedas.code == null)
                            moedas.code = "N/A";
                        if (moedas.symbol == null)
                            moedas.symbol = "N/A";
                    }

                    //Lista sting temporária para dominios
                    List<string> tmpDominios = new List<string>();

                    foreach (var dominios in pais.topLevelDomain)
                    {
                        if (dominios == null)
                        {
                            tmpDominios.Add("N/A");
                        }
                        else
                        {
                            tmpDominios.Add(dominios);
                        }
                    }
                    pais.topLevelDomain = tmpDominios;
                }

                return new Response
                {
                    IsSuccess = true,
                    Result=paises
                };
            }

            catch(Exception ex)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }
    }
}
