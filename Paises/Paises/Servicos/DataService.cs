﻿namespace Paises.Servicos
{
    using Modelos;
    using Servicos;
    using System;
    using System.Collections.Generic;
    using System.Data.SQLite;
    using System.IO;

    public class DataService
    {
        #region Atributos

        private SQLiteConnection connection;

        private SQLiteCommand command;

        private DialogService dialogService;

        #endregion

        #region Contrutores

        public DataService()
        {
            dialogService = new DialogService();

            if (!Directory.Exists("Data"))
            {
                Directory.CreateDirectory("Data");
            }

            var path = @"Data\Paises.sqlite";

            try
            {
                connection = new SQLiteConnection("Data Source=" + path);
                connection.Open();

                // Criar tabela paises
                string sqlcommand = "create table if not exists paises(name text,capital text, region text, population int, area text, flag text, subregion text)";
                command = new SQLiteCommand(sqlcommand, connection);

                // Criar tabela moedas
                sqlcommand =
                    "create table if not exists moedas(idmoeda integer primary key autoincrement, code varchar(250), name varchar(250), symbal char(2), idpais integer, foreign key (idpais) references paises (idpais))";

                command = new SQLiteCommand(sqlcommand, connection);

                command.ExecuteNonQuery();

                //Criar tabela dominios
                sqlcommand =
                    "create table if not exists dominios(iddominio integer primary key autoincrement, dominio varchar(5), idpais integer, foreign key (idpais) references paises (idpais))";

                command = new SQLiteCommand(sqlcommand, connection);
                command.ExecuteNonQuery();

                
            }
            catch (Exception e)
            {
                dialogService.ShowMessage("ERRO", e.Message);
            }
        }

        #endregion

        public void SaveData(List<Pais> pais)
        {
            try
            {
                //guardar tabela Paises
                int idpais = 1;

                foreach (var paises in pais)
                {
                    string sql = string.Format("insert into paises (name, capital, region, population, area, flag, subregion) values (@name,@capital,@region,@population,@area,@flag,@subregion)");
                    // paises.name, paises.capital, paises.region, paises.population);

                    command = new SQLiteCommand(sql, connection);                    
                    command.Parameters.AddWithValue("@name", paises.name);
                    command.Parameters.AddWithValue("@capital", paises.capital);
                    command.Parameters.AddWithValue("@region", paises.region);
                    command.Parameters.AddWithValue("@population", paises.population);
                    command.Parameters.AddWithValue("@area", paises.area);
                    command.Parameters.AddWithValue("@flag", paises.flag);
                    command.Parameters.AddWithValue("@subregion", paises.subregion);

                    command.ExecuteNonQuery();

                    //Guardar dados na tabela moedas
                    foreach (var moeda in paises.currencies)
                    {
                        sql = string.Format("insert into moedas (code, name, symbal) values (@code, @name, @symbal)");

                        command = new SQLiteCommand(sql, connection);
                        command.Parameters.AddWithValue("@code", moeda.code);
                        command.Parameters.AddWithValue("@name", moeda.name);
                        command.Parameters.AddWithValue("@symbal", moeda.symbol);
                        

                        command.ExecuteNonQuery();
                    }

                    //Guardar dados na tabela dominio
                    foreach (var dominio in paises.topLevelDomain)
                    {
                        sql = string.Format("insert into dominios(dominio) values (@dominio)");

                        command = new SQLiteCommand(sql, connection);
                        command.Parameters.AddWithValue("@dominio", dominio);
                        

                        command.ExecuteNonQuery();
                    }


                    idpais++;

                }
                connection.Close();
            }
            catch (Exception e)
            {
                dialogService.ShowMessage("ERRO Save", e.Message);
            }
        }
        public List<Pais> GetData()
        {
            List<Pais> paises = new List<Pais>();

            try
            {
                //string sql = "select name, capital, region, population, area, flag from Paises";

                //command = new SQLiteCommand(sql, connection);
                

                //Saber a quantidade de linhas que existem na tabela
                string sql =
                    "select count(*) from paises";

                command = new SQLiteCommand(sql, connection);

                var linha = command.ExecuteScalar();

                

                //Select tabela paises
                sql = "select  name, capital, region, population from paises";

                command = new SQLiteCommand(sql, connection);

                //Lê cada registo (linha)
                SQLiteDataReader reader = command.ExecuteReader();

                //Carrega da BD para a lista


                while (reader.Read())
                {
                    paises.Add(new Pais
                    {                        
                        name = (string) reader["name"],
                        capital = (string)reader["capital"],
                        region = (string)reader["region"],
                        population = Convert.ToInt32(reader["population"]),
                        area= (string)reader["area"],
                        currencies = LoadMoedas(Convert.ToInt32(reader["idpais"])),
                        topLevelDomain = LoadDominios(Convert.ToInt32(reader["idpais"])),

                    });
                }
                connection.Close();
                return paises;
            }
            catch(Exception e)
            {
                dialogService.ShowMessage("ERRO", e.Message);
                return null;
            }
        }

        public void DeleteData()
        {
            try
            {
                string sql = "delete from Paises";

                command = new SQLiteCommand(sql, connection);

                command.ExecuteNonQuery();
            }
            catch(Exception e)
            {
                dialogService.ShowMessage("Erro", e.Message);
            }
        }

        private List<string> LoadDominios(int IdPais)
        {
            List<string> dominios = new List<string>();

            try
            {
                string sql =
                    "select dominio from dominios where idpais =" + IdPais;

                command = new SQLiteCommand(sql, connection);

                //Lê cada registo (linha)
                SQLiteDataReader reader = command.ExecuteReader();

                //Carrega da BD para a lista
                while (reader.Read())
                {
                    dominios.Add(reader["dominio"].ToString());
                }
                return dominios;
            }
            catch (Exception e)
            {
                dialogService.ShowMessage("Erro LoadDominios", e.Message);

                return null;
            }
        }
        private List<Currency> LoadMoedas(int IdPais)
        {
            List<Currency> moedas = new List<Currency>();

            try
            {
                string sql =
                    "select code, name, symbal from moedas where idpais =" + IdPais;

                command = new SQLiteCommand(sql, connection);

                //Lê cada registo (linha)
                SQLiteDataReader reader = command.ExecuteReader();

                //Carrega da BD para a lista
                while (reader.Read())
                {

                    string symbol;
                    string name;
                    string code;

                    //Verifica se campos são null na BD
                    if (reader["symbal"] == DBNull.Value)
                    {
                        symbol = string.Empty;
                    }
                    else
                    {
                        symbol = (string)reader["symbal"];
                    }
                    if (reader["name"] == DBNull.Value)
                    {
                        name = string.Empty;
                    }
                    else
                    {
                        name = (string)reader["name"];
                    }

                    if (reader["code"] == DBNull.Value)
                    {
                        code = string.Empty;
                    }
                    else
                    {
                        code = (string)reader["code"];
                    }

                    moedas.Add(new Currency
                    {
                        code = code,
                        name = name,
                        symbol = symbol,
                    });
                }
                return moedas;
            }
            catch (Exception e)
            {
                dialogService.ShowMessage("Erro LoadMoedas", e.Message);

                return null;
            }
        }


    }
}
